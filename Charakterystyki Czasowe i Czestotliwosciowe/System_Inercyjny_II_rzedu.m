clear all; close all; clc;
s=tf('s');

% System inercyjny 2 rzędu ---------------------------------------------

k = {1,2,3};
T1 = {1,2,3};
T2 = {1,2,3};
w = {1,2,3};
t = 0:0.1:10;

for i = 1:3

    K1 = k{i}/((T1{i}*s+1)*(T2{i}*s+1));
    K2 = k{i}/((T1{i}*1j*w{i}+1)*(T2{i}*1j*w{i}+1));
    A = abs(K2);
    fi = angle(K2);
    y1 = A*cos(w{i}*t + fi);
    y2 = lsim(K1,cos(w{i}*t),t);
    y3 = cos(w{i}*t);

    figure(i);
    hold on;
    grid on;
    plot(t,y1,'r.');
    plot(t,y2,'g');
    plot(t,y3,'b--')
    xlabel("Czas [s]");
    ylabel("Amplituda");
    title("Porownanie Sygnalow dla danego systemu");
    legend("System inercyjny ii rzedu", "Odpowiedz systemu", "Sygnal wejsciowy cos(wt)");

end

% system dla parametrow o wartosci 2 ---> 2 / 4s^2 4s 1
K1= k{2}/((T1{2}*s+1)*(T2{2}*s+1));
w=0:0.01:10000;
A=abs(2./(4*(1j.*w).^2+4.*w.*1j+1));
fi=angle(2./(4*(1j.*w).^2+4.*w.*1j+1));

figure(4);
grid on;
hold on;
plot(A.*cos(fi),A.*sin(fi),'m');
line([-3 3],[0 0],'Color','k','Linestyle','-')
line([0 0],[-3 3],'Color','k','Linestyle','-')
text(2.8,0.15,'Re');
text(3,0,'>');
text(0.1,2.8,'Im');
text(-0.05,3,'/\');
figure(5)
grid on;
hold on;
%Porównaj do funkcji wbudowanej
nyquist(K1);


 for w = -10:0.1:10

     K1 = (exp(-1j*w*2))*(2/((1j*w)+2));
     figure(6);
     hold on;
     grid on;
     plot(real(K1),imag(K1),'.');
     title("Nyquist Diagram");
     xlabel("Real Part");
     ylabel("Imaginary Part");
     plot3(real(K1),imag(K1),w,'.');
 end
 figure(7)
 K1 = (exp(-s*2))*(2/(s+2));
 nyquist(K1);
 

% System inercyjny 2 rzędu pobudzenie innym obiektem -----------------

k = {1,2,3};
T1 = {1,2,3};
T2 = {1,2,3};
w = {1,2,3};
t = 0:0.1:10;

for i = 1:3

    K1 = k{i}/((T1{i}*s+1)*(T2{i}*s+1));
    K2 = k{i}/((T1{i}*1j*w{i}+1)*(T2{i}*1j*w{i}+1));
    A = abs(K2);
    fi = angle(K2);
    y1 = A*cos(w{i}*t + fi);
    y2 = lsim(K1,cos(w{i}*t),t);
    y3 = 0.1*cos(10*w{i}*t)+cos(w{i}*t);

    figure(i+7);
    hold on;
    grid on;
    plot(t,y1,'r.');
    plot(t,y2,'g');
    plot(t,y3,'b--')
    xlabel("Czas [s]");
    ylabel("Amplituda");
    title("Porownanie Sygnalow dla danego systemu");
    legend("System inercyjny ii rzedu", "Odpowiedz systemu", "Sygnal wejsciowy 0.1cos(10wt)+cos(wt)");

end
