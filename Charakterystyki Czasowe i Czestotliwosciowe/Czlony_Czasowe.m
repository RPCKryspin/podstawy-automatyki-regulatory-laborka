clear all; close all; clc;
s=tf('s');

% 1. Czlon proporcjonalny zmienny parametr b ---------------------------

a = 0;
b = {-2,-0.5, 0.5, 1, 2};
c = 0;
d = 0;
e = 1;

figure(1);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon proporcjonalny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);    

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon proporcjonalny - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

figure(2)
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon proporcjonalny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

figure(3)
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon proporcjonalny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

% 2. Czlon inercyjny zmienny parametr b --------------------------------

a = 0;
b = {5, 2, 1, 0.5, 0.1 };
c = 0;
d = 1;
e = 1;
 
figure(4);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon inercyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y);
 title("Czlon inercyjny - odp. impulsowa ");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

figure(5)
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon inercyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

figure(6)
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon inercyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

% 3. Czlon inercyjny zmienny parametr d --------------------------------

a = 0; 
b = 1;
c = 0;
d = { 5, 2, 1, 0.5, 0.1};
e = 1;

figure(7);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon inercyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y);
 title("Czlon inercyjny - odp. impulsowa ");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(8)
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon inercyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(9)
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon inercyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

% 4. Czlon inercyjny zmienny parametr e --------------------------------

a = 0;
b = 1;
c = 0;
d = 1;
e = {5, 2, 1, 0.5, 0.1};

figure(10);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon inercyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon inercyjny - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

figure(11)
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 pzmap(K);
 hold on;
 grid on;
 title("Czlon inercyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

figure(12)
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 nyquist(K);
 hold on;
 grid on;
 title("Czlon inercyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

% 5. Czlon inercyjny 2 rzedu zmienny parametr b -------------------------

a = 0;
b = 1;
c = 0;
d = { 5, 2, 1, 0.5, 0.1};
e = 1 ;

figure(13);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e)^2;
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon inercyjny 2 rzedu - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e)^2;
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon inercyjny 2 rzedu - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(14)
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e)^2;
 pzmap(K);
 hold on;
 grid on;
 title("Czlon inercyjny 2 rzedu - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(15)
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e)^2;
 nyquist(K);
 hold on;
 grid on;
 title("Czlon inercyjny 2 rzedu - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

% 6. Czlon oscylacyjny zmienny parametr c --------------------------------

a = 0;
c = {5, 2, 1, 0.5, 0.3};
b = 1;
d = 1;
e = 1;

figure(16);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c{i}*s^2+d*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['c = ', num2str(c{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c{i}*s^2+d*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['c = ', num2str(c{i})];
end
legend(legendInfo);

figure(17)
for i = 1:5
 K = a*s+b/(c{i}*s^2+d*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['c = ', num2str(c{i})];
end
legend(legendInfo);

figure(18)
for i = 1:5
 K = a*s+b/(c{i}*s^2+d*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['c = ', num2str(c{i})];
end
legend(legendInfo);

% 7. Czlon oscylacyjny zmienny parametr b --------------------------------

a = 0;
b = {10,5, 2, 1, 0.5};
c = 1;
d = 1;
e = 1;

figure(19);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. impulsowa ");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

figure(20)
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

figure(21)
for i = 1:5
 K = a*s+b{i}/(c*s^2+d*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['b = ', num2str(b{i})];
end
legend(legendInfo);

%8. Czlon oscylacyjny zmienny parametr d --------------------------------

a = 0;
b = 1;
c = 1;
d = {1.1, 1, 0.5, 0.3, 0.2};
e = 1;

figure(22);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(23)
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(24)
for i = 1:5
 K = a*s+b/(c*s^2+d{i}*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

% 9. Czlon oscylacyjny zmienny parametr e -------------------------------

a = 0;
c = 1;
b = 1;
d = 1;
e = {5, 2, 1, 0.5, 0.3};

figure(25);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon oscylacyjny - odp. impulsowa ");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

figure(26)
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 pzmap(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

figure(27)
for i = 1:5
 K = a*s+b/(c*s^2+d*s+e{i});
 nyquist(K);
 hold on;
 grid on;
 title("Czlon oscylacyjny - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

% 10. Człon różniczkujący zmienny parametr a ---------------------------- 

a = {5, 2, 1, 0.5, 0.1};
b = 0;
c = 1; 
d = 1;
e = 1;

figure(28);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a{i}*s/(d*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon różniczkujący - odp. skokowa ");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['a = ', num2str(a{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a{i}*s/(d*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon różniczkujący - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['a = ', num2str(a{i})];
end
legend(legendInfo);

figure(29)
for i = 1:5
 K = a{i}*s/(d*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon różniczkujący - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['a = ', num2str(a{i})];
end
legend(legendInfo);

figure(30)
for i = 1:5
 K = a{i}*s/(d*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon różniczkujący - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['a = ', num2str(a{i})];
end
legend(legendInfo);

% 11. Człon różniczkujący zmienny parametr d --------------------------

a = 1;
b = 0;
c = 0;
d = {5, 2, 1, 0.5, 0.1};
e = 1;

figure(31);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s/(d{i}*s+e);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon różniczkujący - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s/(d{i}*s+e);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon różniczkujący - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(32)
for i = 1:5
 K = a*s/(d{i}*s+e);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon różniczkujący - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(33)
for i = 1:5
 K = a*s/(d{i}*s+e);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon różniczkujący - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

% 12. Człon różniczkujący zmienny parametr e ---------------------------

a = 1;
b = 0;
c = 0;
d = 1;
e = {5, 2, 1, 0.5, 0.1};

figure(34);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a*s/(d*s+e{i});
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon różniczkujący - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a*s/(d*s+e{i});
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon różniczkujący - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

figure(35)
for i = 1:5
 K = a*s/(d*s+e{i});
 pzmap(K);
 hold on;
 grid on;
 title("Czlon różniczkujący - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

figure(36)
for i = 1:5
 K = a*s/(d*s+e{i});
 nyquist(K);
 hold on;
 grid on;
 title("Czlon różniczkujący - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['e = ', num2str(e{i})];
end
legend(legendInfo);

% 13. Człon Calkujacy zmienny parametr d ---------------------------

a = 0;
b = 0; 
c = 0;
d = {5,2,1,0.5,0.1};
e = 0 ;

figure(37);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = 1/(d{i}*s);
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon Calkujacy - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = 1/(d{i}*s);
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon Calkujacy - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(38)
for i = 1:5
 K = 1/(d{i}*s);
 pzmap(K);
 hold on;
 grid on;
 title("Czlon Calkujacy - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(39)
for i = 1:5
 K = 1/(d{i}*s);
 nyquist(K);
 hold on;
 grid on;
 title("Czlon Calkujacy - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

% 14. Człon Calkujacy Rzeczywisty zmienny parametr d --------------------

a = 1;
b = 0; 
c = 0;
d = {5,2,1,0.5,0.1};
e = 1 ;

figure(40);
subplot(211);
hold on;
grid on;
for i = 1:5
 K = a/(s*(d{i}*s+e));
 y = step(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon Calkujacy Rzeczywisty - odp. skokowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

subplot(212);
hold on;
grid on;
for i = 1:5
 K = a/(s*(d{i}*s+e));
 y = impulse(K);
 t = length(y) - 1;
 plot(0:t,y)
 title("Czlon Calkujacy Rzeczywisty - odp. impulsowa");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(41)
for i = 1:5
 K = a/(s*(d{i}*s+e));
 pzmap(K);
 hold on;
 grid on;
 title("Czlon Calkujacy Rzeczywisty - Zera i Bieguny");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);

figure(42)
for i = 1:5
 K = a/(s*(d{i}*s+e));
 nyquist(K);
 hold on;
 grid on;
 title("Czlon Calkujacy Rzeczywisty - Nyquist Diagram");
 xlabel("Czas [s]");
 ylabel("Amplituda");
 legendInfo{i} = ['d = ', num2str(d{i})];
end
legend(legendInfo);
