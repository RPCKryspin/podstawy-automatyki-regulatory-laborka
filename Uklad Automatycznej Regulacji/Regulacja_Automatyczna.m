clear all; close all; clc;
s = tf('s');

%-----------------------------------------------------------

t=0:0.1:25;
k = 2;
T1 = 2;
T2 = 3;
K = (k/s)*(1/(T1*s+1))* 1/(T2*s+1);

%-----------------------------------------------------------

figure(1);
step(K,t);
grid on;
title("Odpowiedz skokowa Obiektu bez regulacji");

%-----------------------------------------------------------

figure(2);
P = 0.1:0.1:40;
MISE = P;
for i = 1:400
    Ke = feedback(1,K*P(i));
    [ei,ti] = step(Ke,P);
    y = ei.^2;
    MISE(i) = sum(y);
end

plot(P,MISE);
axis([0 2 0 10000]);
title("Zależność bledu regulatora P w zalezonosci od jego regulacji")
xlabel('K_p');
ylabel('\int_0^{tk} \xi^2(t;k_p)dt');
grid on;
[x,yi]= min(MISE,[],'linear');
P(yi);

t2=0.1:0.1:75;
figure(3);
KP = feedback(P(yi)*K,1);
step(KP,t2)
title('Odpowiedz Skokowa dla obiektu z dobraną automatyczna regulacja P' );
grid on;
 
%-----------------------------------------------------------

    idx=1;
    P = 0:0.01:0.33;
for i = 1:33 
    Ke=feedback(1,K*P(i));
    [ei,ti]=step(Ke); 
    
    MinE(idx)= min(ei); %wartości minimalna uchybu
    MaxE(idx)=max(ei); %wartości Maksymalna uchybu
    
    przeregulowanie(idx)=abs(MinE(idx)/MaxE(idx))*100; 
    idx=idx+1;
    figure(4);
    plot(ti,ei); % wykresu uchybu od czasu
    hold on;
    grid on; 
    title('Zależnosc uchybudla dobranej serii wartosci Kp');
    xlabel('t');
    ylabel('E(t)');
end

[x,yi] = min(przeregulowanie,[],'linear'); %wyznaczenie najmniejszej wartosci przeregulowania
P(yi);
figure(5);
KP=feedback(K*P(yi),1); %utworzenie transmitancji systemu z nowym regulatorem P
step(KP,t2);
grid on;
title('Kryterium najmniejszego przeregulowania');

%-----------------------------------------------------------


t= 0:1:100;
figure(6);
hold on;
P=0.03;
KP=(P*K)/(1+P*K);
step(KP*12,t);
grid on;
P=0.12;
KP=(P*K)/(1+P*K);
step(KP*12,t);
title("Skok dla napięcia bazowego tokarki z manulanymi nastawami regultora")
xlabel('t');
ylabel('U (voltage)');
legend("P=0.03","P=0.12")

%-----------------------------------------------------------

t= 0:1:1200;
P = [0.33,0.4,0.41,0.42;];
figure(7);
hold on;
for i = 1:4
KP=P(i)*K/(1+P(i)*K);
step(12*KP,t);
grid on;
title("Znaczenie nastawu regulatora P dla Tokarki")
legend("P=0.30","P=0.40","P=0.41","P=0.42")
end

%-----------------------------------------------------------

t=0:0.1:150;
kp = 1;
Ti = [0.5, 1.3, 1.9,2];
P = 0.9;

figure(8);
hold on;
for i = 1:4
I = 1/Ti(i)*s;
KPI = kp*(P+I)*K/(1+(P+I)*K);
step(12*KPI,t);
grid on;
title("Znaczenie nastawu regulatora PI dla Tokarki");
legend("Ti = 0.5","Ti = 1.3","Ti = 1.9","Ti = 2");
end

%-----------------------------------------------------------

t=0:0.1:40;
Td = [1, 2, 1,100];
kp = 1;
Ti = [0.1, 1.5, 1,800];
P = 3.0;
figure(9);
hold on;
for i = 1:4
I = 1/Ti(i)*s;
D = Td(i)*s;
KPID = kp*(P+I+D)*K/(1+(P+I+D)*K);
step(12*KPID,t);
title("Znaczenie nastawu regulatora PID dla Tokarki");
legend("Ti = 0.5, Td = 1 ","Ti = 1.5, Td = 2","Ti = 1, Td = 1","Ti = 800, Td = 100")
grid on;
end

