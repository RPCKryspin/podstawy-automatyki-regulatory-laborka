close all;
close all;
clear all;
k=3;
un=rand(1,20000);
zn=randn(1,20000);
a1=zeros(1,39);
a2=zeros(1,39);
a3=zeros(1,39);
a4=zeros(1,39);
jj=1;
for n=100:500:20000
    
l=n;
m=k;
p=n-9;
phin=un(1:n).';
PHI=zeros(n,k);
a4(jj)=n;
for i=1:n
    
   for j=1:k
      m=m-1;
    if m<l
        PHI(l,m+1)= un(l-m); %%próbki od u1
    else
        PHI(l,m+1)=0; %%próbki przed u1 (przeszłość)
    end
   end
   m=k;
    l=l-1;
end
PHI;


THETA=[2,4,7].'; %%dobieramy jakie chcemy ilośc musi być równa k

Z=zn(1:n).';

Y=PHI*THETA+Z;

Thetaestymowane=(PHI'*PHI)^(-1)*PHI'*Y;
THETATHEMP=abs(Thetaestymowane-THETA);
a1(jj)=THETATHEMP(1);
a2(jj)=THETATHEMP(2);
a3(jj)=THETATHEMP(3);
jj=jj+1;
plot(n,THETATHEMP(1),'x','Color','#77AC30');hold on;
plot(n,THETATHEMP(2),'x','Color','#A2142F');hold on;
plot(n,THETATHEMP(3),'x','Color','#0072BD');hold on;
grid on;
end

plot(a4,a1,'-','Color','#77AC30');hold on;
plot(a4,a2,'-','Color','#A2142F');hold on;
plot(a4,a3,'-','Color','#0072BD');hold on;

ylabel(" ||𝜽est-𝜽||");
xlabel("Ilość Pomiarów n");
legend("Bład b0","Bład b1","Bład b2")


