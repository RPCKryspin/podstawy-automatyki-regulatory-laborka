close all;
close all;
clear all;
clc;
k=3;
n=100;
un=rand(1,n);
zn=randn(1,n);
for k=5:15:70
    
l=n;
m=k;
p=n-9;
phin=un.';
PHI=zeros(n,k);
for i=1:n
    
   for j=1:k
      m=m-1;
    if m<l
        PHI(l,m+1)= un(l-m); %%próbki od u1
    else
        PHI(l,m+1)=0; %%próbki przed u1 (przeszłość)
    end
   end
   m=k;
    l=l-1;
end
PHI;


THETA=(1:k).' ;%%dobieramy jakie chcemy ilośc musi być równa k

Z=zn.';

Y=PHI*THETA+Z;

Thetaestymowane=(PHI'*PHI)^(-1)*PHI'*Y;

thetaonline=zeros(k,1);
P=eye(k);
for j=1:1:n
Y2=PHI*THETA+Z;
phi=PHI(j,1:k)';
P=P-(P*phi*phi'*P)/(1+phi'*P*phi);
En=Y(j)-phi'*thetaonline;
thetaonline=thetaonline+P*phi*En; 
end
thetaonline;
THETATHEMP=abs(thetaonline-THETA);
plot(THETATHEMP);hold on;

grid on;
end
ylabel("||𝜽est-𝜽||");
xlabel("Złożoność dla bledu ");
legend("Bład dla złożoności 5","Bład dla złożoności 20","Bład dla złożoności 35","Bład dla złożoności 50","Bład dla złożoności 65")



