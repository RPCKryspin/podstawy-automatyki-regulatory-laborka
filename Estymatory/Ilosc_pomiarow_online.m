close all;
close all;
clear all;
k=3;
un=rand(1,500);
zn=randn(1,500);
a1=zeros(1,50);
a2=zeros(1,50);
a3=zeros(1,50);
a11=zeros(1,50);
a22=zeros(1,50);
a33=zeros(1,50);
a4=zeros(1,50);
jj=1;
for n=5:10:500
    
l=n;
m=k;
p=n-9;
phin=un(1:n).';
PHI=zeros(n,k);
a4(jj)=n;
for i=1:n
    
   for j=1:k
      m=m-1;
    if m<l
        PHI(l,m+1)= un(l-m); %%próbki od u1
    else
        PHI(l,m+1)=0; %%próbki przed u1 (przeszłość)
    end
   end
   m=k;
    l=l-1;
end
PHI;


THETA=[2,4,7].'; %%dobieramy jakie chcemy ilośc musi być równa k

Z=zn(1:n).';

Y=PHI*THETA+Z;

Thetaestymowane=(PHI'*PHI)^(-1)*PHI'*Y;
thetaonline=[0;0;0];
P=eye(3);

for i=1:1:n
Y2=PHI*THETA+Z;
phi=PHI(i,1:3)';
P=P-(P*phi*phi'*P)/(1+phi'*P*phi);
En=Y(i)-phi'*thetaonline;
thetaonline=thetaonline+P*phi*En; 
end
thetaonline;
THETATHEMP=abs(thetaonline-THETA);
a1(jj)=THETATHEMP(1);
a2(jj)=THETATHEMP(2);
a3(jj)=THETATHEMP(3);
plot(n,THETATHEMP(1),'x','Color','#77AC30');hold on;
plot(n,THETATHEMP(2),'x','Color','#A2142F');hold on;
plot(n,THETATHEMP(3),'x','Color','#0072BD');hold on;
%odkomentowac do porownania obu v

THETATHEMP1=abs(Thetaestymowane-THETA);
a11(jj)=THETATHEMP1(1);
a22(jj)=THETATHEMP1(2);
a33(jj)=THETATHEMP1(3);
jj=jj+1;
plot(n,THETATHEMP1(1),'*','Color','#33520a');hold on;
plot(n,THETATHEMP1(2),'*','Color','#5c0919');hold on;
plot(n,THETATHEMP1(3),'*','Color','#00375c');hold on;




grid on;
end
plot(a4,a11,'--','Color','#33520a');hold on;
plot(a4,a22,'--','Color','#5c0919');hold on;
plot(a4,a33,'--','Color','#00375c');hold on;
plot(a4,a1,'-','Color','#77AC30');hold on;
plot(a4,a2,'-','Color','#A2142F');hold on;
plot(a4,a3,'-','Color','#0072BD');hold on;
ylabel("||𝜽est-𝜽||");
xlabel("ilość pomiarów n");
%legend("Bład b0 (rekurencyjny)","Bład b1 (rekurencyjny)","Bład b2 (rekurencyjny)")
%odkomentowac do porownania obu v
legend("Bład b0 (rekurencyjny)","Bład b1 (rekurencyjny)","Bład b2 (rekurencyjny)","Bład b0","Bład b1","Bład b2")


