close all;
close all;
clear all;
k=3;
n=100;
un=rand(1,n);
zn=randn(1,n);
for k=5:15:70
    
l=n;
m=k;
p=n-9;
phin=un.';
PHI=zeros(n,k);
for i=1:n
    
   for j=1:k
      m=m-1;
    if m<l
        PHI(l,m+1)= un(l-m); %%próbki od u1
    else
        PHI(l,m+1)=0; %%próbki przed u1 (przeszłość)
    end
   end
   m=k;
    l=l-1;
end
PHI;


THETA=(1:k).'; %%dobieramy jakie chcemy ilośc musi być równa k

Z=zn.';

Y=PHI*THETA+Z;

Thetaestymowane=(PHI'*PHI)^(-1)*PHI'*Y;

THETATHEMP=abs(Thetaestymowane-THETA);
plot(THETATHEMP);hold on;

grid on;
end
ylabel("||𝜽est-𝜽||");
xlabel("Złożoność dla bledu");
legend("Bład dla złożoności 5","Bład dla złożoności 20","Bład dla złożoności 35","Bład dla złożoności 50","Bład dla złożoności 65")



